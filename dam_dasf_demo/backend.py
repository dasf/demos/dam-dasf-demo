# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Backend module for dam-dasf-demo."""

import datetime as dt
import os
from typing import Dict, Literal

from demessaging import configure, main, registry  # noqa: F401
from jinja2 import Environment, PackageLoader, select_autoescape

from dam_dasf_demo.frontend_models import TimeSeriesModel
from dam_dasf_demo.types import condatetime
from dam_dasf_demo.vef_widgets import TimeRange, add_widgets

__all__ = ["version_info", "html_info", "compute_statistic"]


def version_info() -> Dict[str, str]:
    """Get the version of the dam-dasf-demo."""
    import dam_dasf_demo

    info = {
        "dam-dasf-demo": dam_dasf_demo.__version__,
    }
    return info


def html_info() -> str:
    """Generate some html formatted information from the backend.

    Returns
    -------
    str
        The HTML info on the backend.
    """
    from demessaging.config import WebsocketURLConfig

    from dam_dasf_demo import __credits__ as credits
    from dam_dasf_demo import __version__ as version

    messaging_config = WebsocketURLConfig(
        topic=os.getenv("DE_BACKEND_TOPIC", "dam-dasf-demo")
    )
    env = Environment(
        loader=PackageLoader("dam_dasf_demo"), autoescape=select_autoescape()
    )
    template = env.get_template("html_info.html")
    return template.render(
        credits=credits,
        version=version,
        topic=messaging_config.topic,
    )


_tmin = dt.datetime.fromisoformat("1979-01-31T18:00:00+00:00")
_tmax = dt.datetime.fromisoformat("1979-05-31T18:00:00+00:00")


@add_widgets(TimeRange(tmin="tmin", tmax="tmax"))
def compute_statistic(
    variable: Literal[
        "Temperature", "Meridional wind-velocity", "Zonal wind-velocity"
    ],
    tmin: condatetime(ge=_tmin, le=_tmax) = _tmin,  # type: ignore
    tmax: condatetime(ge=_tmin, le=_tmax) = _tmax,  # type: ignore
    statistic: Literal["mean", "std"] = "mean",
) -> TimeSeriesModel:
    """Compute some statistic from a local data file

    Parameters
    ----------
    variable : str
        The variable to calculate the statistic for.
    tmin: datetime.datetime
        The lower limit of the time interval to calculate the `statistic` for.
    tmax: datetime.datetime
        The upper limit of the time interval to calculate the `statistic` for.
    statistic: str
        The statistic to calculate.

    Returns
    -------
    TimeSeriesModel
        The computed time series for the specified `variable` with the given
        `statistic` in the given time interval.
    """
    import xarray as xr

    variable_mapping = {
        "Temperature": "t2m",
        "Meridional wind-velocity": "v",
        "Zonal wind-velocity": "u",
    }
    var = variable_mapping[variable]

    demo_file = os.getenv("DAM_DASF_DEMO_FILE")
    if not demo_file:
        raise ValueError(
            "Could not infer location of demo file from DAM_DASF_DEMO_FILE "
            "environment variable."
        )

    # ignore timezone if existing
    tmin = tmin.replace(tzinfo=None)
    tmax = tmax.replace(tzinfo=None)

    ds = xr.open_dataset(demo_file).sel(time=slice(tmin, tmax))
    if statistic == "mean":
        stat = ds[var].mean(["lat", "lon"])
    else:
        stat = ds[var].std(["lat", "lon"])

    times = ds.time.to_pandas().apply(lambda t: t.isoformat()).values.tolist()
    ret = TimeSeriesModel(
        xdata=times,
        ydata={variable: stat.values.tolist()},
        title=ds.attrs["title"],
    )
    ret.layoutOptions["yaxis"] = {
        "title": "%(long_name)s [%(units)s]" % ds[var].attrs
    }
    return ret


if __name__ == "__main__":
    if os.getenv("DE_MESSAGING_ENV_FILE"):
        from demessaging.config import BaseMessagingConfig, ModuleConfig

        config = ModuleConfig(
            messaging_config=BaseMessagingConfig(  # type: ignore[arg-type]
                topic="dam-dasf-demo",
                _env_file=os.environ["DE_MESSAGING_ENV_FILE"],
            )
        )
        main(config=config)
    else:
        main(messaging_config=dict(topic="dam-dasf-demo"))
