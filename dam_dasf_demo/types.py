# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Custom types for demessaging
"""

import datetime as dt
from typing import Annotated, Optional

import annotated_types
from pydantic import Field, Strict


def condatetime(
    *,
    strict: Optional[bool] = None,
    gt: Optional[dt.datetime] = None,
    ge: Optional[dt.datetime] = None,
    lt: Optional[dt.datetime] = None,
    le: Optional[dt.datetime] = None,
) -> type[dt.datetime]:
    extra = {}
    if gt:
        extra["exclusiveMinimum"] = gt
    if lt:
        extra["exclusiveMaximum"] = lt
    if le:
        extra["maximum"] = le
    if ge:
        extra["minimum"] = ge
    return Annotated[  # type: ignore
        dt.datetime,
        Strict(strict) if strict is not None else None,
        annotated_types.Interval(gt=gt, ge=ge, lt=lt, le=le),
        Field(json_schema_extra=extra),  # type: ignore[arg-type]
    ]
