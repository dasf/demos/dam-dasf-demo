# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""
Backend module for dam-dasf-demo.
"""

import datetime as dt
from typing import Callable, Dict, Literal

from demessaging import BackendModule as _BackendModule
from demessaging import main
from demessaging.config import ModuleConfig

import dam_dasf_demo
import dam_dasf_demo.frontend_models
from dam_dasf_demo.types import condatetime

NoneType = type(None)


__all__ = ["version_info", "html_info", "compute_statistic"]


def version_info() -> Dict[str, str]:
    """
    Get the version of the dam-dasf-demo.
    """
    request = {
        "func_name": "version_info",
    }

    model = BackendModule.model_validate(request)
    response = model.compute()

    return response.root  # type: ignore


def html_info() -> str:
    """
    Generate some html formatted information from the backend.

    Returns
    -------
    str
        The HTML info on the backend.
    """
    request = {
        "func_name": "html_info",
    }

    model = BackendModule.model_validate(request)
    response = model.compute()

    return response.root  # type: ignore


_tmin = dt.datetime.fromisoformat("1979-01-31T18:00:00+00:00")
_tmax = dt.datetime.fromisoformat("1979-05-31T18:00:00+00:00")


def compute_statistic(
    variable: Literal[
        "Temperature", "Meridional wind-velocity", "Zonal wind-velocity"
    ],
    tmin: condatetime(ge=_tmin, le=_tmax) = _tmin,  # type: ignore
    tmax: condatetime(ge=_tmin, le=_tmax) = _tmax,  # type: ignore
    statistic: Literal["mean", "std"] = "mean",
) -> dam_dasf_demo.frontend_models.TimeSeriesModel:
    """Compute some statistic from a local data file.

    Parameters
    ----------
    variable : str
        The variable to calculate the statistic for.
    tmin: datetime.datetime
        The lower limit of the time interval to calculate the `statistic` for.
    tmax: datetime.datetime
        The upper limit of the time interval to calculate the `statistic` for.
    statistic: str
        The statistic to calculate.

    Returns
    -------
    TimeSeriesModel
        The computed time series for the specified `variable` with the given
        `statistic` in the given time interval.
    """
    request = {
        "func_name": "compute_statistic",
        "variable": variable,
        "tmin": tmin,
        "tmax": tmax,
        "statistic": statistic,
    }

    model = BackendModule.model_validate(request)
    response = model.compute()

    return response.root  # type: ignore


backend_config = ModuleConfig.model_validate_json(
    """
{
    "messaging_config": {
        "topic": "dam-dasf-demo",
        "max_workers": null,
        "queue_size": null,
        "max_payload_size": 512000,
        "producer_keep_alive": 120,
        "producer_connection_timeout": 30,
        "websocket_url": "wss://datahub.hcdc.hereon.de/ws/dasf",
        "producer_url": "wss://datahub.hcdc.hereon.de/ws/dasf",
        "consumer_url": "wss://datahub.hcdc.hereon.de/ws/dasf"
}
}
"""
)

_creator: Callable
if __name__ == "__main__":
    _creator = main
else:
    _creator = _BackendModule.create_model

BackendModule = _creator(
    __name__,
    config=backend_config,
    class_name="BackendModule",
    members=[version_info, html_info, compute_statistic],
)
