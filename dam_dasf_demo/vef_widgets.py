# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Widgets for the Visual Exploration Framework.

This module defines some widgets that can be used for render custom widgets
in the Visual Exploration Framework from

https://gitlab.awi.de/software-engineering/de.awi.visualexplorer
"""

from typing import Callable, Literal

from demessaging import configure
from pydantic import BaseModel, Field


def add_widgets(*widgets: BaseModel) -> Callable[[Callable], Callable]:
    """A decorator for adding widgets to a function"""

    vef_widgets = [widget.model_dump() for widget in widgets]

    return configure(json_schema_extra={"vefWidgets": vef_widgets})


class TimeRange(BaseModel):
    """A time range for time slider"""

    tmin: str = Field(
        description=(
            "The name of the parameter representing the minimum time value in "
            "the function call."
        )
    )
    tmax: str = Field(
        description=(
            "The name of the parameter representing the maximum time value in "
            "the function call."
        )
    )

    description: str = Field(
        "Select time range", description=("A descriptive label for the slider")
    )

    widget_type: Literal["timerange"] = Field(
        "timerange",
        description=(
            "The identifier for the widget type, used in the frontend"
        ),
    )
