"""Models that can be returned to the Marine Data frontend.

Handlers for these models are implemented in the marine-data frontend
at ``modules/data/dasf/responseHandlerRegistry.js``. The corresponding
handler is selected based on the ``response_type`` field of the model.
"""

# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: Apache-2.0

import datetime as dt
from typing import Any, Dict, List, Literal

from demessaging import configure, main, registry  # noqa: F401
from pydantic import BaseModel


class TimeSeriesModel(BaseModel):
    """A model representing timeseries data."""

    xdata: List[dt.datetime]

    ydata: Dict[str, List[float]]

    title: str = ""

    layoutOptions: Dict[str, Any] = {
        "hovermode": "x",
        "xaxis": {"title": {"text": "Date/Time"}, "autorange": True},
    }

    response_type: Literal["timeseries"] = "timeseries"
