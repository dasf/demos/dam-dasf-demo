# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Test module for :mod:`dam_dasf_demo.backend`."""


def test_show_version():
    from dam_dasf_demo import __version__ as ref_version
    from dam_dasf_demo import backend

    version_info = backend.version_info()
    assert "dam-dasf-demo" in version_info
    assert version_info["dam-dasf-demo"] == ref_version
