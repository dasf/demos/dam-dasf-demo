# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Test file for imports."""


def test_package_import():
    """Test the import of the main package."""
    import dam_dasf_demo  # noqa: F401


def test_backend_import():
    import dam_dasf_demo.backend  # noqa: F401


def test_api_import():
    import dam_dasf_demo.api  # noqa: F401
