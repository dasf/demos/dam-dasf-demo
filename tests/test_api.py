# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Test module for :mod:`dam_dasf_demo.api`."""
from typing import Any

from dam_dasf_demo import api


def test_show_version(connected_module: Any, random_topic: str):
    from dam_dasf_demo import __version__ as ref_version

    version_info = api.version_info()
    assert "dam-dasf-demo" in version_info
    assert version_info["dam-dasf-demo"] == ref_version
