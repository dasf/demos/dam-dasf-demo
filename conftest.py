# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""pytest configuration script for dam-dasf-demo."""
from __future__ import annotations

from typing import TYPE_CHECKING, Callable

import pytest

if TYPE_CHECKING:
    import subprocess as spr

    from dasf_broker.tests.live_ws_server_helper import ChannelsLiveServer


@pytest.fixture
def connected_module(
    db,
    monkeypatch,
    live_ws_server: ChannelsLiveServer,
    connect_module: Callable[[str, str], spr.Popen],
    random_topic: str,
) -> spr.Popen:
    """A process that connects the backend module to the message broker.

    This fixtures uses the ``connect_module`` fixture of the
    ``dasf-broker-django`` package to connect the backend module at
    ``dam_dasf_demo.backend`` to a live server that is
    started at localhost."""
    from demessaging.config import WebsocketURLConfig

    from dam_dasf_demo import api

    process = connect_module(random_topic, "dam_dasf_demo.backend")

    config = WebsocketURLConfig(
        topic=random_topic, websocket_url=live_ws_server.ws_url + "/ws/"
    )
    monkeypatch.setattr(
        api.BackendModule.backend_config, "messaging_config", config
    )

    return process
