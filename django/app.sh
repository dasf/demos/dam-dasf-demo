# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

python manage.py migrate

python manage.py dasf_topic -n dam-dasf-demo --anonymous

python manage.py runserver 0.0.0.0:8080
