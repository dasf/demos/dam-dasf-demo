# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

FROM registry.access.redhat.com/ubi9/python-39

ENV DAM_DASF_DEMO_FILE=/opt/app-root/src/data/demo.nc

# Add application sources to a directory that the assemble script expects them
# and set permissions so that the container runs without root access
USER 0

ADD . /tmp/src
RUN echo ".[backend]" >> /tmp/src/requirements.txt && \
  chmod u+x /tmp/src/app.sh && \
  /usr/bin/fix-permissions /tmp/src

USER 1001

# Install the dependencies
RUN /usr/libexec/s2i/assemble

# Set the default command for the resulting image
CMD /usr/libexec/s2i/run
